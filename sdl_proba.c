#include "SDL.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <time.h>

#define DEBUG 0
#define OPCODE_DEBUG 1


void opcode_state(char *m)
{
  if(OPCODE_DEBUG)
    printf("State : %s\n", m);
}

void msg_printf(char *m){
  if(DEBUG)
    printf("Msg: %s \n", m);
}

unsigned short opcode;

/*od 0x000 to 0xFFF*/
unsigned short I;
unsigned short pc;
unsigned short drawFlag;

 SDL_Surface *screen;
 SDL_Event event;
 int go;

unsigned short stack[16];
unsigned short sp;
/* font set 0x000-0x1FF
 * program ROM and working RAM 0x200-0xFFF
 * */
unsigned char memory[0x1000];
unsigned char V[16];
unsigned char gfx[64*32];
unsigned int key[16];

unsigned char chip8_fontset[80] =
{ 
  0xF0, 0x90, 0x90, 0x90, 0xF0, // 0
  0x20, 0x60, 0x20, 0x20, 0x70, // 1
  0xF0, 0x10, 0xF0, 0x80, 0xF0, // 2
  0xF0, 0x10, 0xF0, 0x10, 0xF0, // 3
  0x90, 0x90, 0xF0, 0x10, 0x10, // 4
  0xF0, 0x80, 0xF0, 0x10, 0xF0, // 5
  0xF0, 0x80, 0xF0, 0x90, 0xF0, // 6
  0xF0, 0x10, 0x20, 0x40, 0x40, // 7
  0xF0, 0x90, 0xF0, 0x90, 0xF0, // 8
  0xF0, 0x90, 0xF0, 0x10, 0xF0, // 9
  0xF0, 0x90, 0xF0, 0x90, 0x90, // A
  0xE0, 0x90, 0xE0, 0x90, 0xE0, // B
  0xF0, 0x80, 0x80, 0x80, 0xF0, // C
  0xE0, 0x90, 0x90, 0x90, 0xE0, // D
  0xF0, 0x80, 0xF0, 0x80, 0xF0, // E
  0xF0, 0x80, 0xF0, 0x80, 0x80  // F
};


unsigned char delay_timer;
unsigned char sound_timer;




/*funkcije*/
void inicijalizacija(){
   int i;
  pc=0x200;
  opcode=0;
  I=0;
  sp=0;
  
  /*Clear display*/
    for(i=0;i<2048;i++)
	   gfx[i]=0;
  /* Initialize the SDL library */
    if( SDL_Init(SDL_INIT_VIDEO) < 0 ) {
        fprintf(stderr,
                "Couldn't initialize SDL: %s\n", SDL_GetError());
        exit(1);
    }
    
    /* Enable Unicode translation */
    SDL_EnableUNICODE( 1 );

    /* Clean up on exit */
    atexit(SDL_Quit);
    
    /*
     * Initialize the display in a 640x480 8-bit palettized mode,
     * requesting a software surface
     */
    screen = SDL_SetVideoMode(640, 320, 32, SDL_SWSURFACE);
    
    if ( screen == NULL ) {
        fprintf(stderr, "Couldn't set 640x480x8 video mode: %s\n",
                        SDL_GetError());
        exit(1);
    }
  /* Clear stack */
    sp=0;
   
   /* Clear register V0-VF*/
   for(i=0;i<=15;i++)
   {
     V[i]=0;
   }
 
  /*Load fontset*/
  for(i=0;i<80;++i)
	memory[i]=chip8_fontset[i];
  
      delay_timer=60;
      sound_timer=5;
}
void ucitavanje_igre(){
   FILE *file;
	char *buffer;
	unsigned long fileLen;

	//Open file
	file = fopen("PONG", "rb");
	if (!file)
	{
		fprintf(stderr, "Unable to open file");
		return;
	}
	
	//Get file length
	fseek(file, 0, SEEK_END);
	fileLen=ftell(file);
	fseek(file, 0, SEEK_SET);

	//Allocate memory
	buffer=(char *)malloc(fileLen+1);
	if (!buffer)
	{
		fprintf(stderr, "Memory error!");
                                fclose(file);
		return;
	}

	//Read file contents into buffer
	fread(buffer, fileLen, 1, file);
	
	int i;
	for(i=0;i<fileLen;i++)
	    memory[i+512]=buffer[i];
	fclose(file);
	free(buffer);
  
}
void ciklus_emulatora(){
  /*FETCH OPCODE*/
  opcode= memory[pc]<<8 | memory[pc+1];
  pc+=2;
  /*DECODE OPCODE*/
  
  switch(opcode & 0xF000)
  {
    case 0x0000:
    {
      switch (opcode)
      {
	/*clear display*/
	case 0x00E0:
	{
	 /*clear_display()*/
	 int i;
	 for(i=0;i<2048;i++)
	   gfx[i]=0;
	 
	 drawFlag=1;
	 opcode_state("0x00E0--clear display");
	  
	}break;
	/*opcode return from subroutine*/
	case 0x00EE:
	{
	 pc= stack[--sp];
	  opcode_state("0x00EE-- return from subroutine");
	}break;
      }
     
    }break;
      /*opcode 0x1nnn 
       *Jump to location nnn*/
    case 0x1000:
    {
      pc=opcode & 0x0FFF;
      opcode_state("0x1000--jump to location");
      
    }break;
    /*opcode 0x2nnn
     call subroutine at nnn*/
    case 0x2000:
    {
      stack[sp]=pc;
      sp++;
      pc=opcode & 0x0FFF;  
      opcode_state("0x2nnn--- call subroutine");
    }break;
    /*0x3Xkk
     skip next instruction if VX equals to kk*/
    case 0x3000:
    {
      if(V[(opcode & 0x0F00)>>8] == (opcode & 0x00FF))
	pc+=2;
      opcode_state("0x3Xkk--skip next instruction if VX equals to kk" );
    }break;
    /*0x4Xkk
     Skinp next instruction if VX not equals to kk*/
    case 0x4000:
    {
	if(V[(opcode & 0x0F00)>>8] != (opcode & 0x00FF))
	  pc+=2;
    opcode_state("0x4Xkk--Skinp next instruction if VX not equals to kk");
    }break;
    /*0x5XY0
     skip next instruction if VX equals VY*/
    case 0x5000:
    {
	if(V[(opcode & 0x0F00)>>8] == V[(opcode & 0x00F0) >> 4])
	  pc +=2;
	opcode_state("0x5XY0---skip next instruction if VX equals VY");
    }break;
    /*0x6Xnn
     set VX equal to nn*/
    case 0x6000:
    {
      V[(opcode & 0x0F00)>>8]=(opcode & 0x00FF);
      opcode_state("0x6Xnn--set VX equal to nn");
    }break;
    /*0x7Xnn
     set VX equal to VX+nn
     sets 1 to VF if val >255-carry flag*/
    case 0x7000:
    {
      unsigned char val=  V[(opcode & 0x0F00)>>8] + (opcode & 0x00FF);
      if(val >255)
      {
	val -=256;
	V[0xF]=1;
      }
      else
      {
	V[0xF]=0;
      }
       V[(opcode & 0x0F00)>>8]=val;
       
       opcode_state("0x7Xnn--set VX equal to VX+nn sets 1 to VF if val >255-carry flag");
    }break;
    /*0x8XYk*/
    case 0x8000:
    {
	switch(opcode & 0xF){
	    /*0xXY0
	    set VX to the value of VY*/
	    case 0x0000:
	    {
	    V[(opcode & 0x0F00)>>8] =V[(opcode & 0x00F0)>>4];
	    opcode_state("0x8XY0--set VX to the value of VY");
	    }break;
	    /*0x8XY1
	    sets VX to VX | VY*/
	    case 0x0001:
	    {
	      V[(opcode & 0x0F00)>>8] |=V[(opcode & 0x00F0)>>4];
	      opcode_state("0x8XY1-- sets VX to VX | VYY");
	    }break;
	    /*0x8XY2
	    sets VX to VX & VY*/
	    case 0x0002:
	    {
	      V[(opcode & 0x0F00)>>8] &=V[(opcode & 0x00F0)>>4];
	     opcode_state("0x8XY2-- sets VX to VX & VY");
	    }break;
	    /*0x8XY3
	    sets VX to VX ^ VY*/
	    case 0x0003:
	    {
	      V[(opcode & 0x0F00)>>8] ^= V[(opcode & 0x00F0)>>4];
	      opcode_state("0x8XY3--sets VX to VX ^ VY");
	    }break;
	    /*0x8XY4
	    sets VX to VX | VY*/
	    case 0x0004:
	    {
	    unsigned char val=  V[(opcode & 0x0F00)>>8] + V[(opcode & 0x00F0)>>4];
		  if(val >255)
		  {
		    val -=256;
		    V[0xF]=1;
		  }
		  else
		  {
		    V[0xF]=0;
		  }
		  V[(opcode & 0x0F00)>>8]=val;
		  opcode_state("0x8XY4--sets VX to VX | VY");
	    }break;
	    /*0x8XY5
	    sets VX to VX-VY.VF is set to 0 when there's a borrow, and 1 when there isn't. */
	    case 0x0005:
	    {
	      unsigned char val=  V[(opcode & 0x0F00)>>8] - V[(opcode & 0x00F0)>>4];
		  if(V[(opcode & 0x0F00)>>8] < V[(opcode & 0x00F0)>>4])
		  {
		    val =256-val;
		    V[0xF]=0;
		  }
		  else
		  {
		    V[0xF]=1;
		  }
		  V[(opcode & 0x0F00)>>8]=val;
	     opcode_state("0x8XY5--sets VX to VX-VY.VF is set to 0 when there's a borrow, and 1 when there isn't.");
	    }break;
	    /*0x8XY5
	    shift VX by 1 to right.VF is set to the value of the least significant bit of VX before the shift*/
	    case 0x0006:
	    {
	      V[0xF]=V[(opcode&0x0F00)>>8] & 0x0001;
	      V[(opcode&0x0F00)>>8]>>1;
	      opcode_state("0x8XY6--shift VX by 1 to right.VF is set to the value of the least significant bit of VX before the shift");
	    }break;
	    /*0x8XY7
	    sets VX to VY-VX.VF is set to 0 when there's a borrow, and 1 when there isn't. */
	    case 0x0007:
	    {
	      unsigned char val=  V[(opcode & 0x00F0)>>4]-V[(opcode & 0x0F00)>>8];
		  if(V[(opcode & 0x0F00)>>8] > V[(opcode & 0x00F0)>>4])
		  {
		    val =256-val;
		    V[0xF]=0;
		  }
		  else
		  {
		    V[0xF]=1;
		  }
		  V[(opcode & 0x0F00)>>8]=val;
	     opcode_state("0x8XY7--sets VX to VY-VX.VF is set to 0 when there's a borrow, and 1 when there isn't.");

	    }break;
	    /*0x8XYE
	    */
	    case 0x000E:
	    {
	      V[0xF]=(V[(opcode&0x0F00)>>8] & 0x80)>>7;
	      V[(opcode&0x0F00)>>8]<<1;
     	      opcode_state("0x8XYF--***");

	    }break;
      }
    }break;
    /*0x9XY0
     skip next instruction if Vx doesn't equal VY*/
    case 0x9000:{
      if(V[(opcode & 0x0F00)>>8] != V[(opcode & 0x00F0) >> 4])
	  pc +=2;
       opcode_state("0x9XY0--skip next instruction if Vx doesn't equal VY");
    } break;
    /*0xANNN
     sets I to address NNN*/
    case 0xA000:
    {
      I=opcode & 0x0FFF;
      opcode_state("0xANNN--sets I to address NNN");

    } break;
    
    /*0xBNNN
     jump to address NNN plus V0*/
    case 0xB000:
    {
      pc= (opcode & 0x0FFF) + V[0];
      opcode_state("0xBNNN--jump to address NNN plus V0");
   
    }break;
    /*0xCXNN
     Sets VX to the result of a bitwise and operation on a random number and NN*/
    case 0xC000:
    {
      srand(time(0));
      V[(opcode & 0x0F00)>>8]= (rand() & 0xFF) & (opcode & 0xFF);
      opcode_state("0xCXNN--Sets VX to the result of a bitwise and operation on a random number and NN");
       }break;
    /*0xDXYN
     Sprites stored in memory at location in index register (I), 8bits wide. Wraps around the screen.
     If when drawn, clears a pixel, register VF is set to 1 otherwise it is zero. All drawing is XOR drawing 
     (i.e. it toggles the screen pixels). Sprites are drawn starting at position VX, VY. N is the number of
     8bit rows that need to be drawn. If N is greater than 1, second line continues at position VX, VY+1, and so on.*/
    case 0xD000:
    {
      unsigned short x = V[(opcode & 0x0F00) >> 8];
      unsigned short y = V[(opcode & 0x00F0) >> 4];
      unsigned short height = opcode & 0x000F;
      unsigned short pixel;

      V[0xF] = 0;
	 
      int yline;
      for (yline = 0; yline < height; yline++)
      {
	pixel = memory[I + yline];
	int xline;
	for(xline = 0; xline < 8; xline++)
	{
	  if((pixel & (0x80 >> xline)) != 0)
	  {
	    if(gfx[(x + xline + ((y + yline) * 64))] == 1)
	      V[0xF] = 1;                                 
	    gfx[x + xline + ((y + yline) * 64)] ^= 1;
	  }
	}
      }
    
      drawFlag = 1; 
       opcode_state("0xDXYN- draw sprites");
    }break;
    case 0xE000:
    {
      switch(opcode & 0x00FF){
      /* EX9E: Skips the next instruction 
	if the key stored in VX is pressed*/
	  case 0x009E:
	    if(key[V[(opcode & 0x0F00) >> 8]] != 0)
	      pc +=2;
	    
	    opcode_state("EX9E: Skips the next instructionif the key stored in VX is pressed.And key is: ");
	    printf("%d",key[V[(opcode & 0x0F00) >> 8]]);
	    fflush(0);
	  break;
       /* EXA1: Skips the next instruction 
     if the key stored in VX isn't pressed*/
	  case 0x00A1:
	    if(key[V[(opcode & 0x0F00) >> 8]] == 0)
	      pc +=2;
	  opcode_state("XA1: Skips the next instruction if the key stored in VX isn't pressed");
	
	  break;
      }
    }break;
    case 0xF000:    
    {
      switch(opcode & 0x00FF){
	/*FX07: Sets VX to the value of the delay timer.*/
	case 0x0007:
	{
	  V[(opcode & 0x0F00) >> 8]=delay_timer;
	  opcode_state("FX07: Sets VX to the value of the delay timer.");
	}break;
	/*A key press is awaited, and then stored in VX.*/
	case 0x000A:
	{
	 
	  while(key[V[(opcode & 0x0F00) >> 8]] != 0){
	    ;
	  }
	    
	    V[(opcode & 0x0F00) >> 8]=key[V[(opcode & 0x0F00) >> 8]];
	    key[V[(opcode & 0x0F00) >> 8]]=0;

	  opcode_state("FX0A: key press is awaited, and then stored in VX.");
	}break;
	case 0x0015:
	{
	  delay_timer=V[(opcode & 0x0F00) >> 8];
	  opcode_state("0xFX15 --set delay_timer from V[x]");
	}break; 
	case 0x0018:
	{
	  sound_timer=V[(opcode & 0x0F00) >> 8];
	  
	  opcode_state("0xFX18 --set sound_timer from V[x]");
	}break;
	case 0x001E:
	{
	  I += V[(opcode & 0x0F00) >> 8]; 
	  
	  opcode_state("0xFX1E --I += V[x]");
	}break;
	case 0x0029:
	{
	  I= V[(opcode & 0x0F00) >> 8]*5;
	  opcode_state("0xFX29 --Sets I to the location of the sprite for the character in VX. Characters 0-F (in hexadecimal) are represented by a 4x5 font");
	}break;
	case 0x0033:
	{
	  memory[I]= V[(opcode & 0x0F00) >> 8]/100;
	  memory[I+1]= (V[(opcode & 0x0F00) >> 8]/10)%10;
	  memory[I+2]= (V[(opcode & 0x0F00) >> 8]%100)%10;
	  
	   opcode_state("0xFX33 --***");
	}break;
	case 0x0055:
	{
	  int i;
	  for(i=0; i<= ((opcode & 0x0F00) >> 8) ;i++)
	  {
	    memory[I+i]=V[i];
	    
	  }
	  
	  opcode_state("0xFX55 --***");
	}break;
	case 0x0065:
	{
	   int i;
	  for(i=0; i<= ((opcode & 0x0F00) >> 8) ;i++)
	  {
	  V[i]= memory[I+i];
	    
	  }
	  
	  opcode_state("0xFX65 --***");
	}break;
      }
      
    }break;
    
    
    default:
      msg_printf("Uknown code\n");
  }
  
  
  /*UPDATE TIMERS*/
       if(delay_timer > 0)
       {
	--delay_timer;
       }
    if(sound_timer > 0)
	{
	  if(sound_timer == 1)
	    msg_printf("BEEP!\n");
	  --sound_timer;
	} 
  
} 
void iscrtavanje(){
  
  Uint32 color;
	Uint32 *pixels = (Uint32 *) screen->pixels;            /* Get the pixels from the Surface*/

        /*Iterrate through the pixels and chagne the color*/
	/*Odraditi funkicju za 10x10*/
        color = SDL_MapRGB(screen->format, 255, 255, 255);
	int *d;
	int i,j,k,l=0;
	for(i=0;i<2048;i++)
	{
	  if(gfx[i]==1){
	    l=(int)(i/64)*640*8+i*10;
	    for(j=0;j < 10; j++)
	    {  
	    if (SDL_MUSTLOCK(screen)) 
	      if (SDL_LockSurface(screen) < 0) 
		return;
	      for(k=0;k<10;k++){
		pixels[l+(j*640)+k]=color;
	      }
	    if (SDL_MUSTLOCK(screen)) 
		SDL_UnlockSurface(screen);
	  
	    }
	   }
	  }
	
           SDL_Flip(screen);
	
	 SDL_UpdateRect(screen, 0, 0, 640, 320);
  
}
void set_keys(int k){
  key[k]=1;
}
void reset_keys(){
  int i;
  for(i=1;i<=16;i++){
    key[i]=0;
  }
}

int catch_key(){
  int c;
  
      while(SDL_PollEvent(&event))
	{
		  switch (event.type)
		  {
			  /* Closing the Window or pressing Escape will exit the program */
			  
			  case SDL_QUIT:
				  exit(0);
			  break;
			  
			  case SDL_KEYDOWN:
				  switch (event.key.keysym.sym)
				  {
				    case SDLK_1:
					    c=1;
					    return c;
				      break;
				    case SDLK_2:
					c=2;
					    return c;
				      
				      break;
				    case SDLK_3:
					c=3;
					    return c;
				      
				      break;
				    case SDLK_4:
					c=4;
					    return c;
				      
				      break;
				    case SDLK_5:
				      c=5;
					    return c;;
				    break;
				    case SDLK_6:
				      c=6;
					    return c;
				    
				    break;
				    case SDLK_7:
				      c=7;
					    return c;
				    
				    break;
				    case SDLK_8:
				      c=8;
					    return c;
				      break;
				      case SDLK_9:
					 c=9;
					    return c;
				      break;
				    case SDLK_q:
					 c=10;
					    return c;
				      
				      break;
				    case SDLK_w:
					 c=11;
					    return c;
				      
				      break;
				    case SDLK_e:
					 c=12;
					    return c;
				      break; 
				    case SDLK_a:
					 c=13;
					    return c;
				      break;
				    case SDLK_s:
					 c=14;
					    return c;
				      
				      break;
				    case SDLK_d:
					 c=15;
					    return c;
				      
				      break;
				  case SDLK_ESCAPE:
					  exit(0);
				  break;
					  
					  default:
					  break;
				  }
			  break;
		  } 
	}
}
int main(int argc, char* argv[]) {

  int i;
 
  
   msg_printf("Inicijalizacija");
  /*Inicijalizacija */
  inicijalizacija();
  msg_printf("ucitavanje_igre");
  /*Load game*/
  ucitavanje_igre();
  
  /* Petlja emulatora*/
  msg_printf("Petlja emulatora");
    for(i=0;;i++){
    /*Pocetak obrade opcode-ova */
    msg_printf(" Obrada kodova\n");
    ciklus_emulatora();
    
    
    /*Provera fleg-a za iscrtavanje */
    if(drawFlag)
    {
    msg_printf("Iscrtavanje");
      iscrtavanje();
     // opcode_state("Iscrtavanje na display");
      drawFlag=0;
    }
   reset_keys();
    int c;
    c=catch_key();
    set_keys(c);
     fflush(0);

    usleep(999);
  }
  
    return 0;
}
